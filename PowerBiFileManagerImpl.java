/**
 * PowerBIFileManager element represents all the files used in the PowerBi zip file.
 */
public class PowerBiFileManagerImpl implements PowerBiFileManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(PowerBiFileManagerImpl.class);
    PowerBINode report;
    ArrayList<ConnectionSSAS>  connectionSSAS = new ArrayList<>();
    PowerQuery query;


    public PowerBiFileManagerImpl(String filename,String shortPath,String serverName,String encoding){

        File f = new File(shortPath);
        String reportName = f.getName();
        String parentName = f.getParent();
        try(
                ZipInputStream in = new ZipInputStream(new FileInputStream(filename))
                ) {

            ZipEntry entry;
            while ((entry = in.getNextEntry()) != null) {
                if (entry.getName().contains("Report/Layout")) {

                    // Read entry from ZipFile with UTF-16LE encoding
                    try( ZipFile file = new ZipFile(filename) ) {
                        InputStream entryStream = file.getInputStream(entry);
                        String jsonText = IOUtils.toString(entryStream, StandardCharsets.UTF_16LE.name());

                        if(query!=null)
                            report = new ReportImpl(jsonText,reportName,parentName,query.getString(),serverName);
                        else
                            report = new ReportImpl(jsonText,reportName,parentName,null,serverName);
                    }
                }
                if (entry.getName().contains("DataMashup")) {
                    byte[] buffer = new byte[1024];
                    int read = 0;
                    ByteArrayOutputStream bo = new ByteArrayOutputStream();
                    while ((read = in.read(buffer, 0, 1024)) >= 0) {
                        bo.write(buffer, 0, read);
                    }
                    byte[] temparray = bo.toByteArray();
                    temparray = Arrays.copyOfRange(temparray, 8, temparray.length - 8);
                    ZipInputStream in2 = new ZipInputStream(new ByteArrayInputStream(temparray));

                    ZipEntry entry2;
                    while ((entry2 = in2.getNextEntry()) != null) {
                        if (entry2.getName().contains("Section1.m")) {
                            StringBuilder s = new StringBuilder();
                            byte[] buffer2 = new byte[1024];
                            int read2 = 0;
                            while ((read2 = in2.read(buffer2, 0, 1024)) >= 0) {
                                s.append(new String(buffer2, 0, read2, Charset.forName(encoding)));
                            }
                            query = new PowerQueryImpl(s.toString());
                            if(report!=null)
                                ((Report)report).setPowerQuerry(query.getString());
                        }
                    }
                }
                if (entry.getName().contains("Connections")) {
                    // Read entry from ZipFile with UTF-8 encoding
                    try( ZipFile file = new ZipFile(filename) ) {
                        InputStream entryStream = file.getInputStream(entry);
                        String jsonText = IOUtils.toString(entryStream, StandardCharsets.UTF_8.name());

                        JSONObject jsonObject = new JSONObject(jsonText);
                        JSONArray jsonArray = jsonObject.getJSONArray("Connections");
                        for (int i = 0; i < jsonArray.length(); i++)
                            connectionSSAS.add(new ConnectionSSASImpl(jsonArray.getJSONObject(i).toString()));
                    }
                }
            }
        } catch (IOException e){
            LOGGER.error("Unable to read content of file {}.", filename, e);
        }
    }

	// getters omitted here
	
}
