/**
 * BMI calculator calculates BMI based on height and weight
 * Calculation is provided by calculation button
 * erase value by clear button.
 */
public class BMIcalculator extends JFrame {

	JLabel heighLabel,heighLabel2, weightLabel,weightLabel2;
	JTextField heightTextField, weightTextField;
	JButton calcButton, clearButton;
	ButtonModel model;

	public static void main(String[] args) {
		new BMIcalculator();
	}
	
	public BMIcalculator() {
		// ... code ommited for exam purpose
	}


	private class ListenForButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == calcButton) {
				JOptionPane.showMessageDialog(BMIcalculator.this, calcBmi() , "The Result of BMI", JOptionPane.INFORMATION_MESSAGE);
			}
			
			if (e.getSource() == clearButton) {
				// set to defaults
				weightTextField.setText("");
				heightTextField.setText("");
			}
		}

		private String calcBmi() {
			String result = "";
			result = validBmi(result);
			
			// calculate only if result is without error
			if (!(result.length()>0)) {
				double height = Double.parseDouble(weightTextField.getText());
				double weight = Double.parseDouble(heightTextField.getText());

				double bmi = (height / (weight*weight));

				if (bmi >= 40) {
					result = "Morbidly Obese";
				} else if(bmi >= 35) {
					result = "Severely Obese";
				}else if(bmi >= 30) {
					result = "Obese";
				}else if(bmi >= 25) {
					result = "Overweight";
				}else if(bmi >= 18.5) {
					result = "Healthy Weight";
				}else {
					result = "Underweight";
				}
				
				result = "Weight Categories: " + result;
			}

			return result;
		}

		private String validBmi(String result) {

			try {
				double height = Double.parseDouble((heightTextField.getText()));
				if (!(height > 0 && height < 2.50)) {
					throw new NumberFormatException();
				}	
			} catch (NumberFormatException e) {
				result += "The height is not valid\n";
			}
			
			try {
				double weight = Double.parseDouble((weightTextField.getText()));
				if (!(weight > 0 && weight < 250)) {
					throw new NumberFormatException();
				}	
			} catch (NumberFormatException e) {
				result += "The weight is not valid\n";
			}

			return result;
		}
	}
	
} // END OF class_simplyCalculator